-------------------------------------------------------------
-- Build file: build.lua
-- Consisted in the LaTeX package `datestamp'
-- Copyright © 2021 निरंजन
--
-- This program is free software: you can redistribute it
-- and/or modify it under the terms of the GNU General
-- Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your
-- option) any later version.
--
-- This program is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the
-- implied warranty of MERCHANTABILITY or FITNESS FOR A
-- PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.
--
-- You should have received a copy of the GNU General Public
-- License along with this program. If not, see
-- <https://www.gnu.org/licenses/>.
-------------------------------------------------------------
module     = "datestamp"
pkgversion = "0.3"
pkgdate    = os.date("%Y-%m-%d")

-- Tagging:
tagfiles = {"datestamp.dtx", "README.txt", "datestamp.ins"}
function update_tag(file, content, tagname, tagdate)
   if tagname == nil then
      tagname = pkgversion
      tagdate = pkgdate
   end
   if string.match(content,"Version:     v%d+%.%d+%w? %(%d+ %a+, %d+%)\n") then
      content = string.gsub(content,"Version:     v%d+%.%d+%w? %(%d+ %a+, %d+%)\n",
            "Version:     v" .. pkgversion .. " (" .. os.date("%d %B, %Y") .. ")\n")
   end
   if string.match(content,"\\def\\datestampversion{%d+%.%d+%w?}\n") then
      content = string.gsub(content,"\\def\\datestampversion{%d+%.%d+%w?}\n",
            "\\def\\datestampversion{" .. pkgversion .. "}\n")
   end
   if string.match(content,"\\def\\datestampdate{%d+-%d+-%d+}\n") then
      content = string.gsub(content,"\\def\\datestampdate{%d+-%d+-%d+}\n",
            "\\def\\datestampdate{" .. pkgdate .. "}\n")
   end
   if string.match(content,"LaTeX Package datestamp v%d+%.%d+%w?\n") then
      content = string.gsub(content,"LaTeX Package datestamp v%d+%.%d+%w?\n",
            "LaTeX Package datestamp v" .. pkgversion .. "\n")
   end
   return content
end

-- Checking:
checkengines = { "luatex" }
stdengine    = "luatex"
checkruns    = 3

-- Documentation:
typesetexe   = "lualatex"
typesetruns  = 2
typesetsuppfiles = { "gfdl-tex.tex", "datestamp-example.ds" }
typesetfiles = { "datestamp.dtx", "datestamp-example.tex" }
docfiles     = { "COPYING" }

-- CTAN upload
ctanreadme    = "README.txt"
uploadconfig  = {
   pkg         = module,
   author      = "निरंजन",
   email       = "hi.niranjan@pm.me",
   uploader    = "निरंजन",
   version     = pkgversion .. " " .. pkgdate,
   license     = "GPLv3+, GFDLv1.3+",
   summary     = "Fixed date-stamps with LuaLaTeX.",
   topic       = "date-time, luatex",
   ctanPath    = "/macros/luatex/latex/datestamp",
   repository  = "http://puszcza.gnu.org.ua/projects/datestamp",
   bugtracker  = "http://puszcza.gnu.org/bugs/?group=datestamp",
   update      = true,
   description = [[This package provides a LuaLaTeX-based method for typesetting fixed dates with the help of an auxiliary file. One can choose to write the custom dates manually in the auxiliary file and the results will be correctly displayed.]]
}
